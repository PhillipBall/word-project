﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Globalization;

namespace Read_From_File
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
          
        }

        private void button1_Click(object sender, EventArgs e)
        {   
            string path = @"C:\Users\Phillip\source\repos\Read From File\Read From File\FileToRead.txt";
            StreamReader st = new StreamReader(path);
            string fd = st.ReadToEnd();
            
            int c = 0;
            string word = "";
            string w2 = "";
            string j;
            var mv = 0;

            char[] vowels = { 'a', 'e', 'i', 'o', 'u' , 'A', 'E', 'I', 'O', 'U'};
            char[] ch = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z' };
            string[] words = fd.Split(new[] { " " }, StringSplitOptions.None);

            Array.Sort(words);

            //method for finding longest word
            foreach (String s in words)
            {
                if (s.Length > c)
                {
                    word = s;
                    c = s.Length;
                }
            }
            textBox1.Text = word.ToLower();
            
            //method for finding word with most vowels
            for (int i=0; i < words.Length; i++)
            {
                var w1 = words[i];
                var nov = 0;
                foreach (var vowel in vowels)
                {
                    if (w1.Contains(vowel)) nov++;
                }

                if (mv < nov)
                {
                    mv = i;
                    w2 = w1;
                }
            }
            textBox2.Text = w2.ToLower();
            //method for finding first word alphabetically -- spent 4 hours on this over complicating the hell out of it.
            
            textBox4.Text = words[0].ToLower();

            //method for finding last word alphabetically -- same with this :/

            j = words.Last();
            textBox3.Text = j.ToLower();

            richTextBox1.Text = fd.ToLower();
            st.Close();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
           
        }
    }
}
